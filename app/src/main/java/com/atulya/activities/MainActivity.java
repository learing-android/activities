package com.atulya.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setTitle("Main Activity");

        Button button3 = findViewById(R.id.button3);
        button3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent3 = new Intent(MainActivity.this, InputTypes.class);
                startActivity(intent3);
            }
        });
    }

    public void launchSettings(View view){
        Intent intent = new Intent(MainActivity.this, SendData.class);
        intent.putExtra("data", "Data from Main Activity");
        startActivity(intent);
    }

    public void buttons(View view){
        Intent forActivity4 = new Intent(this, buttons.class);
        startActivity(forActivity4);
    }

    public void widgets(View view){
        Intent forActivity4 = new Intent(this, Widgets.class);
        startActivity(forActivity4);
    }

    public void widgets2(View view){
        Intent forActivity4 = new Intent(this, Widgets2.class);
        startActivity(forActivity4);
    }

    public void layouts(View view){
        Intent forActivity4 = new Intent(this, Layouts.class);
        startActivity(forActivity4);
    }


}