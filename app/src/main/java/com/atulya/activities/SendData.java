package com.atulya.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class SendData extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.data);

        Intent intent2 = getIntent();
        String data = intent2.getStringExtra("data");
        ((TextView)findViewById(R.id.textView)).setText(data);

//        Button okayButton = findViewById(R.id.button2);
//        okayButton.setOnClickListener(new View.OnClickListener() {
//
//            String name = ((EditText)findViewById(R.id.editPersonName)).getText().toString();
//
//            @Override
//            public void onClick(View view) {
//                Log.d("atul", name);
//            }
//        });


    }

    @Override
    public void onBackPressed() {
        EditText editText = findViewById(R.id.editPersonName);
        String data = editText.getText().toString();
        Intent intent = new Intent();
        intent.putExtra("data", data);
        setResult(1, intent);
    }




    public void takeName(View view){
        String name = ((EditText)findViewById(R.id.editPersonName)).getText().toString();
        Log.d("atul", name);
    }
}