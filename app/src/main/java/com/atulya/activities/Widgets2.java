package com.atulya.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

public class Widgets2 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_widgets2);
        setTitle("Widgets 2");
    }
}